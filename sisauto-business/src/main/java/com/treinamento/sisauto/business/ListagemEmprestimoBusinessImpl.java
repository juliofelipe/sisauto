package com.treinamento.sisauto.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.treinamento.sisauto.domain.business.ListagemEmprestimoBusiness;
import com.treinamento.sisauto.domain.entity.Emprestimo;

@Stateless
public class ListagemEmprestimoBusinessImpl implements ListagemEmprestimoBusiness{
	
	@PersistenceContext(unitName = "sisautoPU")
	private EntityManager em;
	
	public List<Emprestimo> listar() {
		
		return this.em.createQuery("from Emprestimo ", Emprestimo.class).getResultList();
	}

	
}
