package com.treinamento.sisauto.web.listagememprestimos.ListagemEmprestimosViewBean;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;

import com.treinamento.sisauto.domain.business.ListagemEmprestimoBusiness;
import com.treinamento.sisauto.domain.entity.Emprestimo;

@Named
@ViewScoped
public class ListagemEmprestimosViewBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private ListagemEmprestimoBusiness emprestimosBusiness;
	
	private List<Emprestimo> emprestimos;
	
	public void preRenderView (ComponentSystemEvent event) {
		this.emprestimos = this.emprestimosBusiness.listar();
	}
	
	public List<Emprestimo> getEmprestimos() {
		return emprestimos;
	}

	public void setEmprestimos(List<Emprestimo> emprestimos) {
		this.emprestimos = emprestimos;
	}
}
