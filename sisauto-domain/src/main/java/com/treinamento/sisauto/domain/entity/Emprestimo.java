package com.treinamento.sisauto.domain.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "emprestimo")
public class Emprestimo {
	
	@Id
	private Integer id;
	
	private String cpf_cliente;
	
	private Integer id_veiculo;
	
	private String situacao;
	
	private Date previsao_devolucao;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCpf_cliente() {
		return cpf_cliente;
	}

	public void setCpf_cliente(String cpf_cliente) {
		this.cpf_cliente = cpf_cliente;
	}

	public Integer getId_veiculo() {
		return id_veiculo;
	}

	public void setId_veiculo(Integer id_veiculo) {
		this.id_veiculo = id_veiculo;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public Date getPrevisao_devolucao() {
		return previsao_devolucao;
	}

	public void setPrevisao_devolucao(Date previsao_devolucao) {
		this.previsao_devolucao = previsao_devolucao;
	}
}
