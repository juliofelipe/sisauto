package com.treinamento.sisauto.domain.business;

import java.util.List;

import com.treinamento.sisauto.domain.entity.Emprestimo;

public interface ListagemEmprestimoBusiness {
	
	List<Emprestimo> listar();
}
